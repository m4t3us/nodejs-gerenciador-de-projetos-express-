const path = require('path');
const fs = require('fs');

const bodyGitIgnore = function(){
    return 'node_modules';
}

const bodyApiPackageJson = function(){
return `{
    "name": "`+createProjectApi.name+`",
    "version": "0.0.0",
    "private": true,
    "engines": {
        "node": "8.9.3",
        "npm": "3.10.3"
    },
    "scripts": { "start" : "node app.js" },
    "dependencies": {
        "cookie-parser": "~1.4.3",
        "debug": "~2.6.9",
          "ejs": "~2.5.7",
          "express": "^4.16.4",
          "express-ejs-extend": "0.0.1",
          "http-errors": "^1.6.3",
          "morgan": "~1.9.0",
          "mysql": "^2.16.0",
          "jsonwebtoken": "^8.3.0"
    },
    "main": "app.js",
    "author": "",
    "license": "ISC",
    "description": ""
}`;    
}

const bodyPackageJson = function(){
return `{
    "name": "`+createProject.name+`",
    "version": "0.0.0",
    "private": true,
    "engines": {
        "node": "8.9.3",
        "npm": "3.10.3"
    },
    "scripts": { "start" : "node app.js" },
    "dependencies": {
      "cookie-parser": "~1.4.3",
      "debug": "~2.6.9",
      "ejs": "~2.5.7",
      "express": "^4.16.4",
      "express-ejs-extend": "0.0.1",
      "http-errors": "^1.6.3",
      "morgan": "~1.9.0",
      "mysql": "^2.16.0"
    },
    "main": "app.js",
    "author": "",
    "license": "ISC",
    "description": ""
}`;    
}

const bodyApp = function(){
return `const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const port = process.env.PORT || 5000;

//const routerAppTeste = require('./teste/router'); adicione o router do seu app

const app = express();

// view engine setup
app.engine('ejs', require('express-ejs-extend')); // add this line
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'static')));

//app.use('/' , routerAppTeste); defina a rota e o router do seu app

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen( port , ()=> console.log("Servidor ON porta " + port));

module.exports = app;
`;
}

const bodyViewBase = function(){
return `<!DOCTYPE html>
<html>
  <head>
    <title>Nodejs-Express</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel='stylesheet' href='/stylesheets/style.css' />
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark"  style="margin-bottom: 0px;">
          <a class="navbar-brand" href="#">Nodejs</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-item nav-link active" href="">Link1</a>
              <a class="nav-item nav-link" href="">Link2</a>
            </div>
          </div>
    </nav>
    
    <%- content %>
    
  </body>
</html>
`;    
}

const bodyViewError = function(){
return `<div style="padding: 50px; text-align: justify;">
    <h1><%= message %></h1>
    <h2><%= error.status %></h2>
    <pre><%= error.stack %></pre>
</div>
`;
}

const bodyStaticStyle = function(){
return `.jumbotron {
    background: #532F8C;
    color: white;
    padding-bottom: 80px; }
    .jumbotron .btn-primary {
      background: #845ac7;
      border-color: #845ac7; }
      .jumbotron .btn-primary:hover {
        background: #7646c1; }
    .jumbotron p {
      color: #d9ccee;
      max-width: 75%;
      margin: 1em auto 2em; }
    .navbar + .jumbotron {
      margin-top: -20px; }
    .jumbotron .lang-logo {
      display: block;
      background: #B01302;
      border-radius: 50%;
      overflow: hidden;
      width: 100px;
      height: 100px;
      margin: auto;
      border: 2px solid white; }
      .jumbotron .lang-logo img {
        max-width: 100%; }
  
`;
}

const bodyStartServer = function(){
return 'node app.js';
}

const bodyAppRouter = function(){
return `const express = require('express');
const router = express.Router();
const home = require('./index');

router.get('/' , home.exemple);

module.exports = router;
`;
}

const bodyAppIndexJs = function(){
return `const home = {};

home.exemple = function(req,res){
    res.render( __dirname  + '/views/index' , { title:'ExpressViewEJS' });   
};

module.exports = home;
`;
}

const bodyAppIndexHtml = function(){
return `<% extend('../../views/base') %>
<div class="jumbotron text-center" style="margin-top: -5px;">
  <h1>Bem vindo ao <%= title %></h1>
  <a type="button" class="btn btn-lg btn-success"  href="https://nodejs.org/en/docs/"><span class="glyphicon glyphicon-flash"></span> Nodejs </a>
  <a type="button" class="btn btn-lg btn-secondary" href="https://expressjs.com/pt-br/"><span class="glyphicon glyphicon-download"></span> Express </a>
</div>
`;
}

const bodyAppApi = function(){
return `var createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const path = require('path');
const routerApiAuthToken = require('./api-auth-token/router');
const port = process.env.PORT || 5000;
//const routerAppTeste = require('./teste/router'); adicione o router do seu app

const app = express();


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser({}))
app.use(express.static(path.join(__dirname, 'public')));



app.use('/api-auth-token', routerApiAuthToken);
//app.use('/' , routerAppTeste); defina a rota e o router do seu app



app.use(function(req, res, next) {
    next(createError(404));
});


app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.json(
        {
        error : {'Status' : (err.status  || 500) , 'Message' : res.locals.message }
        }   
    ) 
});
  

app.listen( port , ()=> console.log('Servidor Online na porta ' + port));


module.exports = app;
`;
}

const bodyApiTokenAuthLogin = function(){
return `const jwt = require('jsonwebtoken');
const ctrlLogin  = {};

ctrlLogin.login = (req,res)=>{
    const user = {
        id: 1, 
        username: 'm4t3us',
        email: 'allissonmateus89@gmail.com'
    }
    
    jwt.sign({user}, 'secretkey', { expiresIn: '40h' }, (err, token) => {
        res.json({
          token
        });
    });
}

module.exports = ctrlLogin;
`;
}

const bodyApiTokenAuthRouter = function(){
return `const express = require('express');
const router = express.Router();
const ctrlLogin = require('./index');

router.post('/', ctrlLogin.login);

module.exports = router;
`;
}

const bodyApiVerifyHeaderToken = function(){
return `const jwt = require('jsonwebtoken');
var createError = require('http-errors');

const verify_header_token = (req,res,next)=>{

    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        
        jwt.verify(bearerToken, 'secretkey', (err, authData) => {
       
            if(err) {
                console.log('Error Token: ', new Date().toLocaleString() + err );
                next(createError(403));
            }else{
                console.log('Valid Token: ', new Date().toLocaleString() + authData );
                next()
            }

        });      

    } else  next(createError(403))

}

module.exports = verify_header_token;
`;
}

const bodyAppRouterApi = function(){
return `const express = require('express');
const router = express.Router();
const home = require('./index');
const private_router = require('../services/verify-header-token');
//const mysql = require('../services/database.js');

router.get('/',  home.list)
//router.post('/', private_router, home.add) rota com mildware pra verificar a presença do token e a validação

module.exports = router;
`;
}

const bodyAppIndexJsApi = function(){
return `var home = {}

const tittle = 'Bem-vindo API-REST express';

home.list = (req,res)=>{
    res.json(
        tittle
    )
} 


module.exports = home;
`;
}


const bodyDataBaseMysql = function(){
return `const mysql = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : '',
  password : '',
  database : ''
});

connection.connect();

exports.query = function(sql,success,error){
    connection.query( sql , function(err, rows, fields) {
        if (err) error(err)
        else success(rows)
    });
};
`;
}

const createProjectApi = {
    name: '',
    folders: [
        {
            name: 'api-auth-token',
            dir: path.join( __dirname, '/')
        },
        {
            name: 'services',
            dir: path.join( __dirname, '/')
        }      
    ],
    files:[ 
        {
            name:'app.js',
            dir: path.join( __dirname , '/'),
            body: bodyAppApi   
        },
        {
            name:'package.json',
            dir: path.join( __dirname , '/'),
            body: bodyApiPackageJson
        },
        {
            name:'.gitignore',
            dir: path.join( __dirname , '/'),
            body: bodyGitIgnore
        },
        {
            name:'startServer.bat',
            dir: path.join( __dirname , '/'),
            body: bodyStartServer
        },
        {
            name:'router.js',
            dir: path.join( __dirname , '/api-auth-token/'),
            body: bodyApiTokenAuthRouter
        },
        {
            name:'index.js',
            dir: path.join( __dirname , '/api-auth-token/'),
            body: bodyApiTokenAuthLogin
        },
        {
            name:'verify-header-token.js',
            dir: path.join( __dirname , '/services/'),
            body: bodyApiVerifyHeaderToken
        },
        {
            name:'database.js',
            dir: path.join( __dirname , '/services/'),
            body: bodyDataBaseMysql
        }
    ]
}


const createAppApi = {
    name: 'home',
    folders:[
        {
            name: 'home',
            dir: path.join(__dirname , '/'),
        }
    ],
    files:[
        {
            name:'router.js',
            dir: path.join(__dirname , '/'),
            body: bodyAppRouterApi
        },
        {
            name:'index.js',
            dir: path.join(__dirname , '/'),
            body: bodyAppIndexJsApi
        }
    ]
}


const createProject = {
    name: '',
    folders: [
        {
            name:'views',
            dir: path.join( __dirname , '/'),
        },
        {
            name:'static',
            dir: path.join( __dirname , '/'), 
            children: [
                'images',
                'javascripts',
                'stylesheets'
            ]  
        }
    ],
    files: [
        {
            name:'app.js',
            dir: path.join( __dirname , '/'),
            body: bodyApp   
        },
        {
            name:'package.json',
            dir: path.join( __dirname , '/'),
            body: bodyPackageJson
        },
        {
            name:'.gitignore',
            dir: path.join( __dirname , '/'),
            body: bodyGitIgnore
        },
        {
            name:'startServer.bat',
            dir: path.join( __dirname , '/'),
            body: bodyStartServer
        },
        {
            name:'style.css',
            dir: path.join( __dirname , 'static/stylesheets/'),
            body: bodyStaticStyle
        },
        {
            name:'base.ejs',
            dir: path.join( __dirname , 'views/'),
            body: bodyViewBase
        },
        {
            name:'error.ejs',
            dir: path.join( __dirname , 'views/'),
            body: bodyViewError
        },
    ]
};



const createApp = {
    name: 'home',
    folders:[
        {
            name: 'home',
            dir: path.join(__dirname , '/'),
            children: [
                'views'
            ]
        }
    ],
    files:[
        {
            name:'router.js',
            dir: path.join(__dirname , '/'),
            body: bodyAppRouter
        },
        {
            name:'index.js',
            dir: path.join(__dirname , '/'),
            body: bodyAppIndexJs
        },
        {
            name:'index.ejs',
            dir: path.join(__dirname , '/'),
            body: bodyAppIndexHtml
        }
    ]
}

const createFoldersFiles = function(action , folders, files , callback){
    try{
        folders.forEach(folder=>{
            let createDir =  folder.dir + folder.name;
            fs.mkdirSync( createDir );
            if(folder.children){
                folder.children.forEach(children=>{
                    fs.mkdirSync(  path.join( folder.dir + folder.name + '/' , children ) );
                })
            }  
        })

        files.forEach(file=>{ 
            let createFile = file.dir + file.name;
            if(action == 'createApp' || action == 'createAppApi'){
                if(file.name == 'index.ejs')  createFile = file.dir + createApp.name + "/views/" + file.name;
                else if( action == 'createApp' ) createFile = file.dir + createApp.name + "/" + file.name;
                else createFile = file.dir + createAppApi.name + "/" + file.name;;
            }   
            fs.writeFileSync( createFile , file.body() )   
        });

        if(action == 'createProject') callback('Projeto criado com sucesso.');
        else callback('App criado com sucesso.');
    }catch(err){
        callback('Não foi possível criar o app/projeto, talvez o nome já exista ou o projeto já está criado.');
    }
}

const init = function(){
    const param = process.argv.splice(2);
    switch(param[0]){
        case 'help':
            ['createproject "nome"' , 'createapp "nome"' , 'createteproject-api "nome"' , 'createapp-api "nome"'].forEach(value=>{ console.log( value ) });
            break;
        case 'createproject':
            if(param[1])createProject.name = param[1];
            createFoldersFiles( 'createProject' , createProject.folders , createProject.files ,function(data){
               console.log(data)
            })
            break;
        case 'createapp':
            if(param[1]){
                createApp.name = param[1];
                createApp.folders[0].name = param[1];
                createFoldersFiles( 'createApp' , createApp.folders, createApp.files, function(data){console.log(data)})
            }else console.log('É necessário informar o nome do APP. ex: node manage.js createapp "nomeApp"');
            break;
        case 'createproject-api':
            if(param[1])createProjectApi.name = param[1];
            createFoldersFiles( 'createProjectApi' , createProjectApi.folders, createProjectApi.files ,function(data){
                console.log(data)
            })
            break;
        case 'createapp-api':
            if(param[1]){
                createAppApi.name = param[1];
                createAppApi.folders[0].name = param[1];
                createFoldersFiles( 'createAppApi' , createAppApi.folders, createAppApi.files, function(data){console.log(data)})    
            }else console.log('É necessário informar o nome do APP. ex: node manage.js createapp-api "nomeApp"');
            break;
        default:
            console.log('Parametro não encontrado.');    
    }
}

init();


