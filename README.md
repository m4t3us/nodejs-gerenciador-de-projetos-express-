1.*Necessário ter instalado NODEJS e o NPM*

2.Criar um projeto renderizado no servidor com EJS: **node manage.js createproject 'nome'** ou API-REST **node manage.js createproject-api 'nome'**

3.Instalar dependências: **npm install** 

4.Criar um app    : **node manage.js createapp 'nome'** ou APP-API **node manage.js createapp-api 'nome'**

5.Iniciar servidor de desenvolvimento: **node app.js ou clicando no arquivo startserver.bat**